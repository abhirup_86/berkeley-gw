! File hdf5_io.f90 automatically created from hdf5_io.f90p by mako_preprocess.py.
! Do not edit the resulting file (hdf5_io.f90) directly!

!>=========================================================================
!!
!! Module:
!!
!! hdf5_io_m        Originally by FHJ     Last Modified 03/2018 (FHJ)
!!
!!     High-level routines to read and write data in HDF5 format.
!!     This module uses python Mako template to generate Fortran code.
!!     Make sure to run the `mako_preprocess.py` if you edit the source
!!     .f90p file, and don`t edit the generated .f90 directly.
!!
!!=========================================================================

#include "f_defs.h"

module hdf5_io_m
  use, intrinsic :: iso_c_binding
  use global_m
#ifdef HDF5
  use hdf5

  implicit none

  private

  public :: &
    hdf5_read_int, &
    hdf5_read_int_array, &
    hdf5_read_int_hyperslab, &
    hdf5_write_int, &
    hdf5_write_int_array, &
    hdf5_write_int_hyperslab, &
    hdf5_read_double, &
    hdf5_read_double_array, &
    hdf5_read_double_hyperslab, &
    hdf5_write_double, &
    hdf5_write_double_array, &
    hdf5_write_double_hyperslab, &
    hdf5_read_complex, &
    hdf5_read_complex_array, &
    hdf5_read_complex_hyperslab, &
    hdf5_write_complex, &
    hdf5_write_complex_array, &
    hdf5_write_complex_hyperslab, &
    hdf5_read_scalar, &
    hdf5_read_scalar_array, &
    hdf5_read_scalar_hyperslab, &
    hdf5_write_scalar, &
    hdf5_write_scalar_array, &
    hdf5_write_scalar_hyperslab, &
    hdf5_read_logical, &
    hdf5_read_logical_array, &
    hdf5_read_logical_hyperslab, &
    hdf5_write_logical, &
    hdf5_write_logical_array, &
    hdf5_write_logical_hyperslab, &
    hdf5_require_version        , &
    hdf5_require_flavor         , &
    hdf5_create_dset            , &
    hdf5_create_group           , &
    hdf5_exists                 , &
    hdf5_create_file            , &
    hdf5_open_file              , &
    hdf5_close_file             , &
    ptr_complex2real            , &
    ptr_real2complex

  interface hdf5_read_scalar
    module procedure hdf5_read_double, hdf5_read_complex
  end interface
  interface hdf5_read_scalar_array
    module procedure &
hdf5_read_double_array_1, hdf5_read_complex_array_1, &
hdf5_read_double_array_2, hdf5_read_complex_array_2, &
hdf5_read_double_array_3, hdf5_read_complex_array_3, &
hdf5_read_double_array_4, hdf5_read_complex_array_4, &
hdf5_read_double_array_5, hdf5_read_complex_array_5, &
hdf5_read_double_array_6, hdf5_read_complex_array_6, &
hdf5_read_double_array_7, hdf5_read_complex_array_7
  end interface
  interface hdf5_read_scalar_hyperslab
    module procedure &
hdf5_read_double_hyperslab_1, hdf5_read_complex_hyperslab_1, &
hdf5_read_double_hyperslab_2, hdf5_read_complex_hyperslab_2, &
hdf5_read_double_hyperslab_3, hdf5_read_complex_hyperslab_3, &
hdf5_read_double_hyperslab_4, hdf5_read_complex_hyperslab_4, &
hdf5_read_double_hyperslab_5, hdf5_read_complex_hyperslab_5, &
hdf5_read_double_hyperslab_6, hdf5_read_complex_hyperslab_6, &
hdf5_read_double_hyperslab_7, hdf5_read_complex_hyperslab_7
  end interface
  interface hdf5_write_scalar
    module procedure hdf5_write_double, hdf5_write_complex
  end interface
  interface hdf5_write_scalar_array
    module procedure &
hdf5_write_double_array_1, hdf5_write_complex_array_1, &
hdf5_write_double_array_2, hdf5_write_complex_array_2, &
hdf5_write_double_array_3, hdf5_write_complex_array_3, &
hdf5_write_double_array_4, hdf5_write_complex_array_4, &
hdf5_write_double_array_5, hdf5_write_complex_array_5, &
hdf5_write_double_array_6, hdf5_write_complex_array_6, &
hdf5_write_double_array_7, hdf5_write_complex_array_7
  end interface
  interface hdf5_write_scalar_hyperslab
    module procedure &
hdf5_write_double_hyperslab_1, hdf5_write_complex_hyperslab_1, &
hdf5_write_double_hyperslab_2, hdf5_write_complex_hyperslab_2, &
hdf5_write_double_hyperslab_3, hdf5_write_complex_hyperslab_3, &
hdf5_write_double_hyperslab_4, hdf5_write_complex_hyperslab_4, &
hdf5_write_double_hyperslab_5, hdf5_write_complex_hyperslab_5, &
hdf5_write_double_hyperslab_6, hdf5_write_complex_hyperslab_6, &
hdf5_write_double_hyperslab_7, hdf5_write_complex_hyperslab_7
  end interface

contains


!===============================================================================
! Low-level HDF5 routines that capture the error and die, if necessary.
!===============================================================================


subroutine check_error(label, errcode, error_out)
  character(len=*), intent(in) :: label
  integer, intent(in) :: errcode
  integer, intent(out), optional :: error_out

  PUSH_SUB(check_error)

  if (errcode/=0) then
    if (present(error_out)) then
      ! No need to die if the error is being captured.
      if (peinf%verb_debug) then
        write(0,'(3a,i0,".")') 'WARNING: Call to HDF5`s function ', label, ' returned ', errcode
      endif
    else
      ! Error code is not being captured, so die.
      write(0,'(3a,i0,".")') 'ERROR: Call to HDF5`s function ', label, ' returned ', errcode
      call die('Got nonzero error code from HDF5`s function '//trim(label), &
        only_root_writes=.true.)
    endif
  endif
  if (present(error_out)) error_out = errcode

  POP_SUB(check_error)

end subroutine check_error


subroutine bgw_h5screate_simple(rank, dims, space_id, hdferr, maxdims)
  integer, intent(in) :: rank             ! number of dataspace dimensions
  integer(hsize_t), intent(in) :: dims(*) ! array with current dimension sizes
  integer(hid_t), intent(out) :: space_id ! dataspace identifier
  integer, optional, intent(out) :: hdferr! error code
                                          ! 0 on success and -1 on failure
  integer(hsize_t), optional, intent(in) :: maxdims(*)
                                          ! array with the maximum
                                          ! dimension sizes

  integer :: errcode


  PUSH_SUB(bgw_h5screate_simple)
  call h5screate_simple_f(rank, dims, space_id, errcode, maxdims)
  call check_error('h5screate_simple_f', errcode, hdferr)
  POP_SUB(bgw_h5screate_simple)

end subroutine bgw_h5screate_simple


subroutine bgw_h5dcreate(loc_id, name, type_id, space_id, dset_id, &
     hdferr, dcpl_id, lcpl_id, dapl_id)
  integer(hid_t), intent(in) :: loc_id   ! file or group identifier
  character(len=*), intent(in) :: name   ! name of the dataset
  integer(hid_t), intent(in) :: type_id  ! datatype identifier
  integer(hid_t), intent(in) :: space_id ! dataspace identifier
  integer(hid_t), intent(out) :: dset_id ! dataset identifier
  integer, optional, intent(out) :: hdferr! error code
                                         ! 0 on success and -1 on failure
  integer(hid_t), optional, intent(in) :: dcpl_id
                                         ! dataset creation property list
  integer(hid_t), optional, intent(in) :: lcpl_id
                                         ! link creation property list
  integer(hid_t), optional, intent(in) :: dapl_id
                                         ! dataset access property list

  integer :: errcode

  PUSH_SUB(bgw_h5dcreate)
  call h5dcreate_f(loc_id, name, type_id, space_id, dset_id, &
     errcode, dcpl_id, lcpl_id, dapl_id)
  call check_error('h5dcreate_f', errcode, hdferr)
  POP_SUB(bgw_h5dcreate)

end subroutine bgw_h5dcreate


subroutine bgw_h5dopen(loc_id, name, dset_id, hdferr, dapl_id)
  integer(hid_t), intent(in) :: loc_id   ! file or group identifier
  character(len=*), intent(in) :: name   ! name of the dataset
  integer(hid_t), intent(out) :: dset_id ! dataset identifier
  integer, optional, intent(out) :: hdferr         ! error code:
                                         ! 0 on success and -1 on failure
  integer(hid_t), optional, intent(in) :: dapl_id
                                         ! dataset access property list

  integer :: errcode

  PUSH_SUB(bgw_h5dopen)
  call h5dopen_f(loc_id, name, dset_id, errcode, dapl_id)
  call check_error('h5dcreate_f', errcode, hdferr)
  POP_SUB(bgw_h5dopen)

end subroutine bgw_h5dopen


subroutine bgw_h5dclose(dset_id, hdferr)
  integer(hid_t), intent(in) :: dset_id
  integer, optional, intent(out) :: hdferr

  integer :: errcode

  PUSH_SUB(bgw_h5dclose)
  call h5dclose_f(dset_id, errcode)
  call check_error('h5dclose_f', errcode, hdferr)
  POP_SUB(bgw_h5dclose)

end subroutine bgw_h5dclose


subroutine bgw_h5dget_space(dataset_id, dataspace_id, hdferr)
  integer(hid_t), intent(in) :: dataset_id      ! dataset identifier
  integer(hid_t), intent(out) :: dataspace_id   ! dataspace identifier
  integer, optional, intent(out) :: hdferr       ! error code
                                                ! 0 on success and -1 on failure

  integer :: errcode

  PUSH_SUB(bgw_h5dget_space)
  call h5dget_space_f(dataset_id, dataspace_id, errcode)
  call check_error('h5dget_space_f', errcode, hdferr)
  POP_SUB(bgw_h5dget_space)

end subroutine bgw_h5dget_space


subroutine bgw_h5sselect_hyperslab(space_id, operator, start, count, &
  hdferr, stride, block)
  integer(hid_t), intent(in) :: space_id  ! dataspace identifier
  integer, intent(in) :: operator         ! flag, valid values are:
                                          !    h5s_select_set_f
                                          !    h5s_select_or_f
  integer(hsize_t), dimension(*), intent(in) :: start
                                          ! offset of start of hyperslab
  integer(hsize_t), dimension(*), intent(in) :: count
                                          ! number of blocks to select
                                          ! from dataspace
  integer, optional, intent(out) :: hdferr! error code
                                          ! 0 on success and -1 on failure
  integer(hsize_t), dimension(:), optional, intent(in) :: stride
                                          ! array of how many elements to
                                          ! move in each direction
  integer(hsize_t), dimension(:), optional, intent(in) :: block
                                          ! size of the element block

  integer :: errcode

  PUSH_SUB(bgw_h5sselect_hyperslab)
  call h5sselect_hyperslab_f(space_id, operator, start, count, errcode, &
                             stride, block)
  call check_error('h5sselect_hyperslab_f', errcode, hdferr)
  POP_SUB(bgw_h5sselect_hyperslab)

end subroutine bgw_h5sselect_hyperslab


subroutine bgw_h5sclose(space_id, hdferr)
  integer(hid_t), intent(in) :: space_id
  integer, optional, intent(out) :: hdferr

  integer :: errcode

  PUSH_SUB(bgw_h5sclose)
  call h5sclose_f(space_id, errcode)
  call check_error('h5sclose_f', errcode, hdferr)
  POP_SUB(bgw_h5sclose)

end subroutine bgw_h5sclose


!===============================================================================


!> Return a pointer that is a complex reinterpretation of the double precision
!! input array data_in. No data is copied if the input array is contiguous.
!! `sz` is the total number of elements of the input array.
function ptr_complex2real(data_in, sz) result(data_out)
  complex(DPC), intent(inout), target :: data_in(*)
  integer, intent(in) :: sz
  real(DP), pointer :: data_out(:)

  call c_f_pointer(c_loc(data_in), data_out, [2*sz])

end function ptr_complex2real


!> Return a pointer that is a double precision reinterpretation of the complex
!! input array data_in. No data is copied if the input array is contiguous.
!! `sz` is the total number of elements of the input array.
function ptr_real2complex(data_in, sz) result(data_out)
  real(DP), intent(inout), target :: data_in(*)
  integer, intent(in) :: sz
  complex(DPC), pointer :: data_out(:)

  call c_f_pointer(c_loc(data_in), data_out, [sz/2])

end function ptr_real2complex


!===============================================================================
! High-level HDF5 functions
!===============================================================================


!> Make sure that an hdf5 file has the correct version number.
subroutine hdf5_require_version(loc_id, dset_name, req_version, fname, allow_greater)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in) :: req_version !< version to require
  character(LEN=*), intent(in) :: fname !< file name, for debugging purposes
  !> allow the file to have a version greater than req_version? Defaults to .false.
  logical, intent(in), optional :: allow_greater

  integer :: file_version, errcode
  logical :: allow_greater_

  PUSH_SUB(hdf5_require_version)

  file_version = -1
  call hdf5_read_int(loc_id, dset_name, file_version, errcode)
  allow_greater_ = .false.
  if (present(allow_greater)) allow_greater_ = allow_greater
  if (file_version<req_version .or. &
    (file_version/=req_version.and..not.allow_greater_) .or. errcode/=0) then
    if (peinf%inode==0) then
      write(0,*)
      write(0,*) 'ERROR: Incorrect version in file ', trim(fname),' while reading ',trim(dset_name)
      write(0,*) '       Expecting: ', req_version
      write(0,*) '       Got: ', file_version
      write(0,*) '       Errcode: ', errcode
      write(0,*) 'Your file was probably generated by an older version of BerkeleyGW and'
      write(0,*) 'is now obsolete. Consult the documentation and use the appropriate converter.'
      write(0,*)
    endif
    call die("Wrong version for file '"+trim(fname)+"'.", only_root_writes=.true.)
  endif

  POP_SUB(hdf5_require_version)

end subroutine hdf5_require_version


!> Make sure that an hdf5 file has the correct flavor.
subroutine hdf5_require_flavor(loc_id, dset_name, req_flavor, fname)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in) :: req_flavor !< flavor to require
  character(LEN=*), intent(in) :: fname !< file name, for debugging purposes

  integer :: file_flavor, errcode

  PUSH_SUB(hdf5_require_flavor)

  file_flavor = -1
  call hdf5_read_int(loc_id, dset_name, file_flavor, errcode)
  if (file_flavor/=req_flavor.or.errcode/=0) then
    if (peinf%inode==0) then
      write(0,*)
      write(0,*) 'ERROR: Incorrect flavor in file ', trim(fname), ' while reading ',trim(dset_name)
      write(0,*) '       Expecting: ', req_flavor
      write(0,*) '       Got: ', file_flavor
      write(0,*) '       Errcode: ', errcode
      write(0,*) 'You are probably linking the wrong file or running the BerkeleyGW binary with'
      write(0,*) 'the wrong flavor.'
      write(0,*)
    endif
    call die("Wrong flavor in file "+trim(fname)+"'.", only_root_writes=.true.)
  endif

  POP_SUB(hdf5_require_flavor)

end subroutine hdf5_require_flavor


subroutine hdf5_create_file(filename, file_id, parallel_io, comm, error)
  character(len=*), intent(in) :: filename
  integer(hid_t), intent(out) :: file_id
  logical, optional, intent(in) :: parallel_io
  integer, optional, intent(in) :: comm
  integer, optional, intent(out) :: error

  integer :: errcode, comm_
#ifdef MPI
  integer(HID_T) :: plist_id
#endif
  logical :: parallel_io_

  PUSH_SUB(hdf5_create_file)

  parallel_io_ = .false.
  if (present(parallel_io)) parallel_io_ = parallel_io
#ifdef MPI
  comm_ = MPI_COMM_WORLD
  if (present(comm)) comm_ = comm
#else
  comm_ = 0
#endif

#ifdef MPI
  if (parallel_io_) then
    call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, errcode)
    call check_error('h5pcreate_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif

    call h5pset_fapl_mpio_f(plist_id, comm_, MPI_INFO_NULL, errcode)
    call check_error('h5pset_fapl_mpio_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif

    call h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, errcode, access_prp=plist_id)
    call check_error('h5fcreate_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif

    call h5pclose_f(plist_id, errcode)
    call check_error('h5pclose_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif
  else
#endif
    call h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, errcode)
    call check_error('h5fcreate_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif
#ifdef MPI
  endif
#endif
  POP_SUB(hdf5_create_file)

end subroutine hdf5_create_file


subroutine hdf5_open_file(filename, mode, file_id, parallel_io, comm, error)
  character(len=*), intent(in) :: filename
  character(len=*), intent(in) :: mode !< r, w, rw = r+
  integer(hid_t), intent(out) :: file_id
  logical, optional, intent(in) :: parallel_io
  integer, optional, intent(in) :: comm
  integer, optional, intent(out) :: error

  integer :: errcode, access_flag, comm_
#ifdef MPI
  integer(HID_T) :: plist_id
#endif
  logical :: parallel_io_, exists

  PUSH_SUB(hdf5_open_file)

  select case(mode)
    case ('w', 'W')
      call hdf5_create_file(filename, file_id, parallel_io, comm, error)
      POP_SUB(hdf5_open_file)
      return
   case ('r', 'R')
      access_flag = H5F_ACC_RDONLY_F
   case ('r+', 'R+', 'rw', 'RW')
      access_flag = H5F_ACC_RDWR_F
   case default
      call die('Unknown HDF5 open mode: '//mode, only_root_writes=.true.)
  end select
  parallel_io_ = .false.
  if (present(parallel_io)) parallel_io_ = parallel_io
#ifdef MPI
  comm_ = MPI_COMM_WORLD
  if (present(comm)) comm_ = comm
#else
  comm_ = 0
#endif

  inquire(file=filename, exist=exists)
  if (.not.exists) then
    if (present(error)) then
      error = -1
      POP_SUB(hdf5_open_file)
    endif
    call die('Input HDF5 file "'//filename//'" does not exist.', only_root_writes=.true.)
  endif

#ifdef MPI
  if (parallel_io_) then
    call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, errcode)
    call check_error('h5pcreate_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif

    call h5pset_fapl_mpio_f(plist_id, comm_, MPI_INFO_NULL, errcode)
    call check_error('h5pset_fapl_mpio_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif

    call h5fopen_f(filename, access_flag, file_id, errcode, access_prp=plist_id)
    call check_error('h5fopen_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif

    call h5pclose_f(plist_id, errcode)
    call check_error('h5pclose_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif
  else
#endif
    call h5fopen_f(filename, access_flag, file_id, errcode)
    call check_error('h5fopen_f', errcode, error)
    if (present(error)) then; if (error/=0) return; endif
#ifdef MPI
  endif
#endif
  POP_SUB(hdf5_open_file)

end subroutine hdf5_open_file


subroutine hdf5_close_file(file_id, error)
  integer(hid_t), intent(inout) :: file_id
  integer, optional, intent(out) :: error

  integer :: errcode

  call h5fclose_f(file_id, errcode)
  call check_error('h5fclose_f', errcode, error)
  file_id = -1

end subroutine hdf5_close_file


!> Create an empty dataset.
subroutine hdf5_create_dset(loc_id, dset_name, dtype, dims, error)
  integer(HID_T), intent(in) :: loc_id !< hdf5 file id
  character(LEN=*), intent(in) :: dset_name !< hdf5 dataset name
  integer(HID_T), intent(in) :: dtype !< hdf5 data type
  integer, intent(in) :: dims(:) !< dimensions of the array
  integer, intent(out), optional :: error !< error code

  integer(HSIZE_T) :: hdims(size(dims))
  integer(HID_T) :: dset_id
  integer(HID_T) :: dspace

  PUSH_SUB(hdf5_create_dset)

  hdims(:) = dims(:)
  call bgw_h5screate_simple(size(dims), hdims, dspace, error)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5dcreate(loc_id, dset_name, dtype, dspace, dset_id, error)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5dclose(dset_id, error)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(dspace, error)
  if (present(error)) then; if (error/=0) return; endif

  POP_SUB(hdf5_create_dset)

end subroutine hdf5_create_dset


!> Create an empty group.
subroutine hdf5_create_group(loc_id, group_name, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: group_name !< HDF5 group name
  integer, optional, intent(out) :: error

  integer(HID_T) :: group_id
  integer :: errcode

  PUSH_SUB(hdf5_create_group)

  call h5gcreate_f(loc_id, group_name, group_id, errcode)
  call check_error('h5gcreate_f', errcode, error)
  if (present(error)) then; if (error/=0) return; endif
  call h5gclose_f(group_id, errcode)
  call check_error('h5gclose_f', errcode, error)
  if (present(error)) then; if (error/=0) return; endif

  POP_SUB(hdf5_create_group)

end subroutine hdf5_create_group


logical function hdf5_exists(loc_id, link_name)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: link_name !< HDF5 group/dataset name

  integer :: error

  call h5lexists_f(loc_id, link_name, hdf5_exists, error)
  if (error/=0) hdf5_exists = .false.

end function hdf5_exists


!######################################
!# Beginning of high-level interfaces #
!######################################


!-------------------------------
! Routines for int data type
!-------------------------------

!> read int value (rank-0 array) to/from an HDF5 file.
subroutine hdf5_read_int(loc_id, dset_name, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(inout), target :: buf !< data buffer
  integer, intent(out), optional :: error !< HDF5 error code

  integer, pointer :: buf_1d(:)

  PUSH_SUB(hdf5_read_int)
  call c_f_pointer(c_loc(buf), buf_1d, [1])
  call hdf5_read_int_lowlevel(loc_id, dset_name, [-1_HSIZE_T], buf_1d, error=error)
  POP_SUB(hdf5_read_int)

end subroutine hdf5_read_int

!> read int array to/from an HDF5 file.
subroutine hdf5_read_int_array(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  integer, intent(inout), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_int_array)
  call hdf5_read_int_lowlevel(loc_id, dset_name, int(dims,kind=HSIZE_T), buf, error=error)
  POP_SUB(hdf5_read_int_array)

end subroutine hdf5_read_int_array

!> read section (hyperslab) of int array to/from an HDF5 file.
subroutine hdf5_read_int_hyperslab(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  integer, intent(inout), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_int_hyperslab)
  call hdf5_read_int_lowlevel(loc_id, dset_name, int(read_count,kind=HSIZE_T), buf, error, offsetf=offset)
  POP_SUB(hdf5_read_int_hyperslab)

end subroutine hdf5_read_int_hyperslab

!> write int value (rank-0 array) to/from an HDF5 file.
subroutine hdf5_write_int(loc_id, dset_name, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), target :: buf !< data buffer
  integer, intent(out), optional :: error !< HDF5 error code

  integer, pointer :: buf_1d(:)

  PUSH_SUB(hdf5_write_int)
  call c_f_pointer(c_loc(buf), buf_1d, [1])
  call hdf5_write_int_lowlevel(loc_id, dset_name, [-1_HSIZE_T], buf_1d, error=error)
  POP_SUB(hdf5_write_int)

end subroutine hdf5_write_int

!> write int array to/from an HDF5 file.
subroutine hdf5_write_int_array(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  integer, intent(in), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_int_array)
  call hdf5_write_int_lowlevel(loc_id, dset_name, int(dims,kind=HSIZE_T), buf, error=error)
  POP_SUB(hdf5_write_int_array)

end subroutine hdf5_write_int_array

!> write section (hyperslab) of int array to/from an HDF5 file.
subroutine hdf5_write_int_hyperslab(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  integer, intent(in), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_int_hyperslab)
  call hdf5_write_int_lowlevel(loc_id, dset_name, int(read_count,kind=HSIZE_T), buf, error, offsetf=offset)
  POP_SUB(hdf5_write_int_hyperslab)

end subroutine hdf5_write_int_hyperslab

!-------------------------------
! Routines for double data type
!-------------------------------

!> read double value (rank-0 array) to/from an HDF5 file.
subroutine hdf5_read_double(loc_id, dset_name, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  real(DP), intent(inout), target :: buf !< data buffer
  integer, intent(out), optional :: error !< HDF5 error code

  real(DP), pointer :: buf_1d(:)

  PUSH_SUB(hdf5_read_double)
  call c_f_pointer(c_loc(buf), buf_1d, [1])
  call hdf5_read_double_lowlevel(loc_id, dset_name, [-1_HSIZE_T], buf_1d, error=error)
  POP_SUB(hdf5_read_double)

end subroutine hdf5_read_double

!> read double array to/from an HDF5 file.
subroutine hdf5_read_double_array(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(inout), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_array)
  call hdf5_read_double_lowlevel(loc_id, dset_name, int(dims,kind=HSIZE_T), buf, error=error)
  POP_SUB(hdf5_read_double_array)

end subroutine hdf5_read_double_array

!> read section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_read_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(inout), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_hyperslab)
  call hdf5_read_double_lowlevel(loc_id, dset_name, int(read_count,kind=HSIZE_T), buf, error, offsetf=offset)
  POP_SUB(hdf5_read_double_hyperslab)

end subroutine hdf5_read_double_hyperslab

!> write double value (rank-0 array) to/from an HDF5 file.
subroutine hdf5_write_double(loc_id, dset_name, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  real(DP), intent(in), target :: buf !< data buffer
  integer, intent(out), optional :: error !< HDF5 error code

  real(DP), pointer :: buf_1d(:)

  PUSH_SUB(hdf5_write_double)
  call c_f_pointer(c_loc(buf), buf_1d, [1])
  call hdf5_write_double_lowlevel(loc_id, dset_name, [-1_HSIZE_T], buf_1d, error=error)
  POP_SUB(hdf5_write_double)

end subroutine hdf5_write_double

!> write double array to/from an HDF5 file.
subroutine hdf5_write_double_array(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(in), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_array)
  call hdf5_write_double_lowlevel(loc_id, dset_name, int(dims,kind=HSIZE_T), buf, error=error)
  POP_SUB(hdf5_write_double_array)

end subroutine hdf5_write_double_array

!> write section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_write_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(in), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_hyperslab)
  call hdf5_write_double_lowlevel(loc_id, dset_name, int(read_count,kind=HSIZE_T), buf, error, offsetf=offset)
  POP_SUB(hdf5_write_double_hyperslab)

end subroutine hdf5_write_double_hyperslab

!-------------------------------
! Routines for complex data type
!-------------------------------

!> read complex value (rank-0 array) to/from an HDF5 file.
subroutine hdf5_read_complex(loc_id, dset_name, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  complex(DPC), intent(inout), target :: buf !< data buffer
  integer, intent(out), optional :: error !< HDF5 error code

  complex(DPC), pointer :: buf_1d(:)

  PUSH_SUB(hdf5_read_complex)
  call c_f_pointer(c_loc(buf), buf_1d, [1])
  call hdf5_read_complex_lowlevel(loc_id, dset_name, [-1_HSIZE_T], buf_1d, error=error)
  POP_SUB(hdf5_read_complex)

end subroutine hdf5_read_complex

!> read complex array to/from an HDF5 file.
subroutine hdf5_read_complex_array(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(inout), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_array)
  call hdf5_read_complex_lowlevel(loc_id, dset_name, int(dims,kind=HSIZE_T), buf, error=error)
  POP_SUB(hdf5_read_complex_array)

end subroutine hdf5_read_complex_array

!> read section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_read_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(inout), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_hyperslab)
  call hdf5_read_complex_lowlevel(loc_id, dset_name, int(read_count,kind=HSIZE_T), buf, error, offsetf=offset)
  POP_SUB(hdf5_read_complex_hyperslab)

end subroutine hdf5_read_complex_hyperslab

!> write complex value (rank-0 array) to/from an HDF5 file.
subroutine hdf5_write_complex(loc_id, dset_name, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  complex(DPC), intent(in), target :: buf !< data buffer
  integer, intent(out), optional :: error !< HDF5 error code

  complex(DPC), pointer :: buf_1d(:)

  PUSH_SUB(hdf5_write_complex)
  call c_f_pointer(c_loc(buf), buf_1d, [1])
  call hdf5_write_complex_lowlevel(loc_id, dset_name, [-1_HSIZE_T], buf_1d, error=error)
  POP_SUB(hdf5_write_complex)

end subroutine hdf5_write_complex

!> write complex array to/from an HDF5 file.
subroutine hdf5_write_complex_array(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(in), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_array)
  call hdf5_write_complex_lowlevel(loc_id, dset_name, int(dims,kind=HSIZE_T), buf, error=error)
  POP_SUB(hdf5_write_complex_array)

end subroutine hdf5_write_complex_array

!> write section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_write_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(in), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_hyperslab)
  call hdf5_write_complex_lowlevel(loc_id, dset_name, int(read_count,kind=HSIZE_T), buf, error, offsetf=offset)
  POP_SUB(hdf5_write_complex_hyperslab)

end subroutine hdf5_write_complex_hyperslab

!-------------------------------
! Routines for logical data type
!-------------------------------

!> read logical value (rank-0 array) to/from an HDF5 file.
subroutine hdf5_read_logical(loc_id, dset_name, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  logical, intent(inout), target :: buf !< data buffer
  integer, intent(out), optional :: error !< HDF5 error code

  logical, pointer :: buf_1d(:)

  PUSH_SUB(hdf5_read_logical)
  call c_f_pointer(c_loc(buf), buf_1d, [1])
  call hdf5_read_logical_lowlevel(loc_id, dset_name, [-1_HSIZE_T], buf_1d, error=error)
  POP_SUB(hdf5_read_logical)

end subroutine hdf5_read_logical

!> read logical array to/from an HDF5 file.
subroutine hdf5_read_logical_array(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  logical, intent(inout), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_logical_array)
  call hdf5_read_logical_lowlevel(loc_id, dset_name, int(dims,kind=HSIZE_T), buf, error=error)
  POP_SUB(hdf5_read_logical_array)

end subroutine hdf5_read_logical_array

!> read section (hyperslab) of logical array to/from an HDF5 file.
subroutine hdf5_read_logical_hyperslab(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  logical, intent(inout), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_logical_hyperslab)
  call hdf5_read_logical_lowlevel(loc_id, dset_name, int(read_count,kind=HSIZE_T), buf, error, offsetf=offset)
  POP_SUB(hdf5_read_logical_hyperslab)

end subroutine hdf5_read_logical_hyperslab

!> write logical value (rank-0 array) to/from an HDF5 file.
subroutine hdf5_write_logical(loc_id, dset_name, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  logical, intent(in), target :: buf !< data buffer
  integer, intent(out), optional :: error !< HDF5 error code

  logical, pointer :: buf_1d(:)

  PUSH_SUB(hdf5_write_logical)
  call c_f_pointer(c_loc(buf), buf_1d, [1])
  call hdf5_write_logical_lowlevel(loc_id, dset_name, [-1_HSIZE_T], buf_1d, error=error)
  POP_SUB(hdf5_write_logical)

end subroutine hdf5_write_logical

!> write logical array to/from an HDF5 file.
subroutine hdf5_write_logical_array(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  logical, intent(in), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_logical_array)
  call hdf5_write_logical_lowlevel(loc_id, dset_name, int(dims,kind=HSIZE_T), buf, error=error)
  POP_SUB(hdf5_write_logical_array)

end subroutine hdf5_write_logical_array

!> write section (hyperslab) of logical array to/from an HDF5 file.
subroutine hdf5_write_logical_hyperslab(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  logical, intent(in), dimension(*) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_logical_hyperslab)
  call hdf5_write_logical_lowlevel(loc_id, dset_name, int(read_count,kind=HSIZE_T), buf, error, offsetf=offset)
  POP_SUB(hdf5_write_logical_hyperslab)

end subroutine hdf5_write_logical_hyperslab

!################################
!# End of high-level interfaces #
!################################


!#####################################################
!# Intermediate routines for ASSUMED-SHAPE arguments #
!#####################################################


!> read double array to/from an HDF5 file.
subroutine hdf5_read_double_array_1(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(inout), dimension(:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_array_1)
  call hdf5_read_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_double_array_1)

end subroutine hdf5_read_double_array_1

!> read section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_read_double_hyperslab_1(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(inout), dimension(:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_hyperslab_1)
  call hdf5_read_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_double_hyperslab_1)

end subroutine hdf5_read_double_hyperslab_1

!> read double array to/from an HDF5 file.
subroutine hdf5_read_double_array_2(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(inout), dimension(:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_array_2)
  call hdf5_read_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_double_array_2)

end subroutine hdf5_read_double_array_2

!> read section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_read_double_hyperslab_2(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(inout), dimension(:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_hyperslab_2)
  call hdf5_read_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_double_hyperslab_2)

end subroutine hdf5_read_double_hyperslab_2

!> read double array to/from an HDF5 file.
subroutine hdf5_read_double_array_3(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(inout), dimension(:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_array_3)
  call hdf5_read_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_double_array_3)

end subroutine hdf5_read_double_array_3

!> read section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_read_double_hyperslab_3(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(inout), dimension(:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_hyperslab_3)
  call hdf5_read_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_double_hyperslab_3)

end subroutine hdf5_read_double_hyperslab_3

!> read double array to/from an HDF5 file.
subroutine hdf5_read_double_array_4(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(inout), dimension(:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_array_4)
  call hdf5_read_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_double_array_4)

end subroutine hdf5_read_double_array_4

!> read section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_read_double_hyperslab_4(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(inout), dimension(:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_hyperslab_4)
  call hdf5_read_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_double_hyperslab_4)

end subroutine hdf5_read_double_hyperslab_4

!> read double array to/from an HDF5 file.
subroutine hdf5_read_double_array_5(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(inout), dimension(:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_array_5)
  call hdf5_read_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_double_array_5)

end subroutine hdf5_read_double_array_5

!> read section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_read_double_hyperslab_5(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(inout), dimension(:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_hyperslab_5)
  call hdf5_read_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_double_hyperslab_5)

end subroutine hdf5_read_double_hyperslab_5

!> read double array to/from an HDF5 file.
subroutine hdf5_read_double_array_6(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(inout), dimension(:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_array_6)
  call hdf5_read_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_double_array_6)

end subroutine hdf5_read_double_array_6

!> read section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_read_double_hyperslab_6(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(inout), dimension(:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_hyperslab_6)
  call hdf5_read_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_double_hyperslab_6)

end subroutine hdf5_read_double_hyperslab_6

!> read double array to/from an HDF5 file.
subroutine hdf5_read_double_array_7(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(inout), dimension(:,:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_array_7)
  call hdf5_read_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_double_array_7)

end subroutine hdf5_read_double_array_7

!> read section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_read_double_hyperslab_7(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(inout), dimension(:,:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_double_hyperslab_7)
  call hdf5_read_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_double_hyperslab_7)

end subroutine hdf5_read_double_hyperslab_7

!> write double array to/from an HDF5 file.
subroutine hdf5_write_double_array_1(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(in), dimension(:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_array_1)
  call hdf5_write_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_double_array_1)

end subroutine hdf5_write_double_array_1

!> write section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_write_double_hyperslab_1(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(in), dimension(:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_hyperslab_1)
  call hdf5_write_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_double_hyperslab_1)

end subroutine hdf5_write_double_hyperslab_1

!> write double array to/from an HDF5 file.
subroutine hdf5_write_double_array_2(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(in), dimension(:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_array_2)
  call hdf5_write_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_double_array_2)

end subroutine hdf5_write_double_array_2

!> write section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_write_double_hyperslab_2(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(in), dimension(:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_hyperslab_2)
  call hdf5_write_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_double_hyperslab_2)

end subroutine hdf5_write_double_hyperslab_2

!> write double array to/from an HDF5 file.
subroutine hdf5_write_double_array_3(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(in), dimension(:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_array_3)
  call hdf5_write_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_double_array_3)

end subroutine hdf5_write_double_array_3

!> write section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_write_double_hyperslab_3(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(in), dimension(:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_hyperslab_3)
  call hdf5_write_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_double_hyperslab_3)

end subroutine hdf5_write_double_hyperslab_3

!> write double array to/from an HDF5 file.
subroutine hdf5_write_double_array_4(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(in), dimension(:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_array_4)
  call hdf5_write_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_double_array_4)

end subroutine hdf5_write_double_array_4

!> write section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_write_double_hyperslab_4(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(in), dimension(:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_hyperslab_4)
  call hdf5_write_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_double_hyperslab_4)

end subroutine hdf5_write_double_hyperslab_4

!> write double array to/from an HDF5 file.
subroutine hdf5_write_double_array_5(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(in), dimension(:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_array_5)
  call hdf5_write_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_double_array_5)

end subroutine hdf5_write_double_array_5

!> write section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_write_double_hyperslab_5(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(in), dimension(:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_hyperslab_5)
  call hdf5_write_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_double_hyperslab_5)

end subroutine hdf5_write_double_hyperslab_5

!> write double array to/from an HDF5 file.
subroutine hdf5_write_double_array_6(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(in), dimension(:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_array_6)
  call hdf5_write_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_double_array_6)

end subroutine hdf5_write_double_array_6

!> write section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_write_double_hyperslab_6(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(in), dimension(:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_hyperslab_6)
  call hdf5_write_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_double_hyperslab_6)

end subroutine hdf5_write_double_hyperslab_6

!> write double array to/from an HDF5 file.
subroutine hdf5_write_double_array_7(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  real(DP), intent(in), dimension(:,:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_array_7)
  call hdf5_write_double_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_double_array_7)

end subroutine hdf5_write_double_array_7

!> write section (hyperslab) of double array to/from an HDF5 file.
subroutine hdf5_write_double_hyperslab_7(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  real(DP), intent(in), dimension(:,:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_double_hyperslab_7)
  call hdf5_write_double_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_double_hyperslab_7)

end subroutine hdf5_write_double_hyperslab_7

!> read complex array to/from an HDF5 file.
subroutine hdf5_read_complex_array_1(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(inout), dimension(:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_array_1)
  call hdf5_read_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_complex_array_1)

end subroutine hdf5_read_complex_array_1

!> read section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_read_complex_hyperslab_1(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(inout), dimension(:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_hyperslab_1)
  call hdf5_read_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_complex_hyperslab_1)

end subroutine hdf5_read_complex_hyperslab_1

!> read complex array to/from an HDF5 file.
subroutine hdf5_read_complex_array_2(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(inout), dimension(:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_array_2)
  call hdf5_read_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_complex_array_2)

end subroutine hdf5_read_complex_array_2

!> read section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_read_complex_hyperslab_2(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(inout), dimension(:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_hyperslab_2)
  call hdf5_read_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_complex_hyperslab_2)

end subroutine hdf5_read_complex_hyperslab_2

!> read complex array to/from an HDF5 file.
subroutine hdf5_read_complex_array_3(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(inout), dimension(:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_array_3)
  call hdf5_read_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_complex_array_3)

end subroutine hdf5_read_complex_array_3

!> read section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_read_complex_hyperslab_3(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(inout), dimension(:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_hyperslab_3)
  call hdf5_read_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_complex_hyperslab_3)

end subroutine hdf5_read_complex_hyperslab_3

!> read complex array to/from an HDF5 file.
subroutine hdf5_read_complex_array_4(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(inout), dimension(:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_array_4)
  call hdf5_read_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_complex_array_4)

end subroutine hdf5_read_complex_array_4

!> read section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_read_complex_hyperslab_4(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(inout), dimension(:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_hyperslab_4)
  call hdf5_read_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_complex_hyperslab_4)

end subroutine hdf5_read_complex_hyperslab_4

!> read complex array to/from an HDF5 file.
subroutine hdf5_read_complex_array_5(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(inout), dimension(:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_array_5)
  call hdf5_read_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_complex_array_5)

end subroutine hdf5_read_complex_array_5

!> read section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_read_complex_hyperslab_5(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(inout), dimension(:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_hyperslab_5)
  call hdf5_read_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_complex_hyperslab_5)

end subroutine hdf5_read_complex_hyperslab_5

!> read complex array to/from an HDF5 file.
subroutine hdf5_read_complex_array_6(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(inout), dimension(:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_array_6)
  call hdf5_read_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_complex_array_6)

end subroutine hdf5_read_complex_array_6

!> read section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_read_complex_hyperslab_6(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(inout), dimension(:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_hyperslab_6)
  call hdf5_read_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_complex_hyperslab_6)

end subroutine hdf5_read_complex_hyperslab_6

!> read complex array to/from an HDF5 file.
subroutine hdf5_read_complex_array_7(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(inout), dimension(:,:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_array_7)
  call hdf5_read_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_read_complex_array_7)

end subroutine hdf5_read_complex_array_7

!> read section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_read_complex_hyperslab_7(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(inout), dimension(:,:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_read_complex_hyperslab_7)
  call hdf5_read_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_read_complex_hyperslab_7)

end subroutine hdf5_read_complex_hyperslab_7

!> write complex array to/from an HDF5 file.
subroutine hdf5_write_complex_array_1(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(in), dimension(:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_array_1)
  call hdf5_write_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_complex_array_1)

end subroutine hdf5_write_complex_array_1

!> write section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_write_complex_hyperslab_1(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(in), dimension(:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_hyperslab_1)
  call hdf5_write_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_complex_hyperslab_1)

end subroutine hdf5_write_complex_hyperslab_1

!> write complex array to/from an HDF5 file.
subroutine hdf5_write_complex_array_2(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(in), dimension(:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_array_2)
  call hdf5_write_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_complex_array_2)

end subroutine hdf5_write_complex_array_2

!> write section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_write_complex_hyperslab_2(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(in), dimension(:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_hyperslab_2)
  call hdf5_write_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_complex_hyperslab_2)

end subroutine hdf5_write_complex_hyperslab_2

!> write complex array to/from an HDF5 file.
subroutine hdf5_write_complex_array_3(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(in), dimension(:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_array_3)
  call hdf5_write_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_complex_array_3)

end subroutine hdf5_write_complex_array_3

!> write section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_write_complex_hyperslab_3(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(in), dimension(:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_hyperslab_3)
  call hdf5_write_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_complex_hyperslab_3)

end subroutine hdf5_write_complex_hyperslab_3

!> write complex array to/from an HDF5 file.
subroutine hdf5_write_complex_array_4(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(in), dimension(:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_array_4)
  call hdf5_write_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_complex_array_4)

end subroutine hdf5_write_complex_array_4

!> write section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_write_complex_hyperslab_4(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(in), dimension(:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_hyperslab_4)
  call hdf5_write_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_complex_hyperslab_4)

end subroutine hdf5_write_complex_hyperslab_4

!> write complex array to/from an HDF5 file.
subroutine hdf5_write_complex_array_5(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(in), dimension(:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_array_5)
  call hdf5_write_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_complex_array_5)

end subroutine hdf5_write_complex_array_5

!> write section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_write_complex_hyperslab_5(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(in), dimension(:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_hyperslab_5)
  call hdf5_write_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_complex_hyperslab_5)

end subroutine hdf5_write_complex_hyperslab_5

!> write complex array to/from an HDF5 file.
subroutine hdf5_write_complex_array_6(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(in), dimension(:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_array_6)
  call hdf5_write_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_complex_array_6)

end subroutine hdf5_write_complex_array_6

!> write section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_write_complex_hyperslab_6(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(in), dimension(:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_hyperslab_6)
  call hdf5_write_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_complex_hyperslab_6)

end subroutine hdf5_write_complex_hyperslab_6

!> write complex array to/from an HDF5 file.
subroutine hdf5_write_complex_array_7(loc_id, dset_name, dims, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  integer, intent(in), dimension(:) :: dims !< size of the buffer buf
  complex(DPC), intent(in), dimension(:,:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_array_7)
  call hdf5_write_complex_array(loc_id, dset_name, dims, buf, error=error)
  POP_SUB(hdf5_write_complex_array_7)

end subroutine hdf5_write_complex_array_7

!> write section (hyperslab) of complex array to/from an HDF5 file.
subroutine hdf5_write_complex_hyperslab_7(loc_id, dset_name, read_count, offset, buf, error)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(len=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !> This does not need to be the same as the size of the dataset.
  integer, intent(in) :: read_count(:)
  !> Offset when reading dataset from file.
  integer, intent(in) :: offset(:)
  complex(DPC), intent(in), dimension(:,:,:,:,:,:,:) :: buf !< data buffer
  integer, intent(out), optional :: error !< error code

  PUSH_SUB(hdf5_write_complex_hyperslab_7)
  call hdf5_write_complex_hyperslab(loc_id, dset_name, read_count, offset, buf, error=error)
  POP_SUB(hdf5_write_complex_hyperslab_7)

end subroutine hdf5_write_complex_hyperslab_7

!############################################################
!# End of intermediate routines for ASSUMED-SHAPE arguments #
!############################################################

!###############################################
!# LOW LEVEL routines -- INT + DOUBLE versions #
!###############################################


!> read int to/from an HDF5 file.
subroutine hdf5_read_int_lowlevel(loc_id, dset_name, countf, buf, error, offsetf)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !! Pass (/-1/) to read/write a scalar rank-0 array.
  integer(HSIZE_T), intent(in) :: countf(:)
  !> Data buffer. We treat it as a flat contiguous 1D array.
  integer, intent(inout), dimension(*) :: buf
  integer, intent(out), optional :: error !< error code
  !> Offset when reading dataset from file.
  integer, intent(in), optional :: offsetf(:)

  integer(HSIZE_T) :: hcountf(size(countf)) !< Count for file dataspace
  integer(HSIZE_T) :: hcountm(1) !< Count for memory dataspace
  integer(HSIZE_T) :: hoffsetf(size(countf)) !< Offset for file filespace_id
  integer(HID_T) :: dset_id, filespace_id, memspace_id
  integer :: errcode, rank
  logical :: exists

  PUSH_SUB(hdf5_read_int_lowlevel)

  if (present(error)) error = 0
  call h5lexists_f(loc_id, dset_name, exists, errcode)
  call check_error('h5lexists_f', merge(0,1,exists.and.errcode==0), error)
  if (present(error)) then; if (error/=0) return; endif

  ! FHJ: Create or get file dataspace
  if (present(offsetf)) then
    hoffsetf(:) = offsetf(:)
  else
    hoffsetf(:) = 0
  endif
  hcountf(:) = countf(:)
  rank = size(hcountf)
  if (any(countf<1)) then
    rank = 0
    ! Note: hcountf and hoffsetf are not referenced if rank==0
  endif

  if (.not.exists) then
    ! FHJ: Create file dataspace
    call bgw_h5screate_simple(rank, hcountf, filespace_id, error)
    if (present(error)) then; if (error/=0) return; endif
    ! FHJ: Create dataset
    call bgw_h5dcreate(loc_id, dset_name, H5T_NATIVE_INTEGER, filespace_id, dset_id, error)
    if (present(error)) then; if (error/=0) return; endif
  else
    ! FHJ: Open dataset
    call bgw_h5dopen(loc_id, dset_name, dset_id, error)
    if (present(error)) then; if (error/=0) return; endif
    ! FHJ: Get file dataspace
    call bgw_h5dget_space(dset_id, filespace_id, error)
    if (present(error)) then; if (error/=0) return; endif
  endif

  ! FHJ: Select hyperslab from file
  if (rank>0) then
    call bgw_h5sselect_hyperslab(filespace_id, H5S_SELECT_SET_F, hoffsetf, hcountf, error)
    if (present(error)) then; if (error/=0) return; endif
  endif

  ! FHJ: Create flat memory filespace_id
  hcountm(1) = max(1, product(countf))
  call bgw_h5screate_simple(1, hcountm, memspace_id, error)
  if (present(error)) then; if (error/=0) return; endif

  ! FHJ: read filespace_id
  call h5dread_f(dset_id, H5T_NATIVE_INTEGER, buf, hcountm, errcode, &
    memspace_id, filespace_id)
  call check_error('h5dread_f', errcode, error)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(memspace_id, errcode)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(filespace_id, errcode)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5dclose(dset_id, errcode)
  if (present(error)) then; if (error/=0) return; endif

  POP_SUB(hdf5_read_int_lowlevel)

end subroutine hdf5_read_int_lowlevel


!> write int to/from an HDF5 file.
subroutine hdf5_write_int_lowlevel(loc_id, dset_name, countf, buf, error, offsetf)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !! Pass (/-1/) to read/write a scalar rank-0 array.
  integer(HSIZE_T), intent(in) :: countf(:)
  !> Data buffer. We treat it as a flat contiguous 1D array.
  integer, intent(in), dimension(*) :: buf
  integer, intent(out), optional :: error !< error code
  !> Offset when reading dataset from file.
  integer, intent(in), optional :: offsetf(:)

  integer(HSIZE_T) :: hcountf(size(countf)) !< Count for file dataspace
  integer(HSIZE_T) :: hcountm(1) !< Count for memory dataspace
  integer(HSIZE_T) :: hoffsetf(size(countf)) !< Offset for file filespace_id
  integer(HID_T) :: dset_id, filespace_id, memspace_id
  integer :: errcode, rank
  logical :: exists

  PUSH_SUB(hdf5_write_int_lowlevel)

  if (present(error)) error = 0
  call h5lexists_f(loc_id, dset_name, exists, errcode)
  if (.not.exists .and. present(offsetf)) then
    ! FHJ: The developer should manually create the dataset, because countf
    ! is *NOT* the total size of the dataset, so we don`t have enough into to
    ! create the dataset.
    call die('Internal error: cannot automatically create dataset with "offsetf" argument.', &
      only_root_writes=.true.)
  endif

  ! FHJ: Create or get file dataspace
  if (present(offsetf)) then
    hoffsetf(:) = offsetf(:)
  else
    hoffsetf(:) = 0
  endif
  hcountf(:) = countf(:)
  rank = size(hcountf)
  if (any(countf<1)) then
    rank = 0
    ! Note: hcountf and hoffsetf are not referenced if rank==0
  endif

  if (.not.exists) then
    ! FHJ: Create file dataspace
    call bgw_h5screate_simple(rank, hcountf, filespace_id, error)
    if (present(error)) then; if (error/=0) return; endif
    ! FHJ: Create dataset
    call bgw_h5dcreate(loc_id, dset_name, H5T_NATIVE_INTEGER, filespace_id, dset_id, error)
    if (present(error)) then; if (error/=0) return; endif
  else
    ! FHJ: Open dataset
    call bgw_h5dopen(loc_id, dset_name, dset_id, error)
    if (present(error)) then; if (error/=0) return; endif
    ! FHJ: Get file dataspace
    call bgw_h5dget_space(dset_id, filespace_id, error)
    if (present(error)) then; if (error/=0) return; endif
  endif

  ! FHJ: Select hyperslab from file
  if (rank>0) then
    call bgw_h5sselect_hyperslab(filespace_id, H5S_SELECT_SET_F, hoffsetf, hcountf, error)
    if (present(error)) then; if (error/=0) return; endif
  endif

  ! FHJ: Create flat memory filespace_id
  hcountm(1) = max(1, product(countf))
  call bgw_h5screate_simple(1, hcountm, memspace_id, error)
  if (present(error)) then; if (error/=0) return; endif

  ! FHJ: write filespace_id
  call h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, buf, hcountm, errcode, &
    memspace_id, filespace_id)
  call check_error('h5dwrite_f', errcode, error)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(memspace_id, errcode)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(filespace_id, errcode)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5dclose(dset_id, errcode)
  if (present(error)) then; if (error/=0) return; endif

  POP_SUB(hdf5_write_int_lowlevel)

end subroutine hdf5_write_int_lowlevel


!> read double to/from an HDF5 file.
subroutine hdf5_read_double_lowlevel(loc_id, dset_name, countf, buf, error, offsetf)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !! Pass (/-1/) to read/write a scalar rank-0 array.
  integer(HSIZE_T), intent(in) :: countf(:)
  !> Data buffer. We treat it as a flat contiguous 1D array.
  real(DP), intent(inout), dimension(*) :: buf
  integer, intent(out), optional :: error !< error code
  !> Offset when reading dataset from file.
  integer, intent(in), optional :: offsetf(:)

  integer(HSIZE_T) :: hcountf(size(countf)) !< Count for file dataspace
  integer(HSIZE_T) :: hcountm(1) !< Count for memory dataspace
  integer(HSIZE_T) :: hoffsetf(size(countf)) !< Offset for file filespace_id
  integer(HID_T) :: dset_id, filespace_id, memspace_id
  integer :: errcode, rank
  logical :: exists

  PUSH_SUB(hdf5_read_double_lowlevel)

  if (present(error)) error = 0
  call h5lexists_f(loc_id, dset_name, exists, errcode)
  call check_error('h5lexists_f', merge(0,1,exists.and.errcode==0), error)
  if (present(error)) then; if (error/=0) return; endif

  ! FHJ: Create or get file dataspace
  if (present(offsetf)) then
    hoffsetf(:) = offsetf(:)
  else
    hoffsetf(:) = 0
  endif
  hcountf(:) = countf(:)
  rank = size(hcountf)
  if (any(countf<1)) then
    rank = 0
    ! Note: hcountf and hoffsetf are not referenced if rank==0
  endif

  if (.not.exists) then
    ! FHJ: Create file dataspace
    call bgw_h5screate_simple(rank, hcountf, filespace_id, error)
    if (present(error)) then; if (error/=0) return; endif
    ! FHJ: Create dataset
    call bgw_h5dcreate(loc_id, dset_name, H5T_NATIVE_DOUBLE, filespace_id, dset_id, error)
    if (present(error)) then; if (error/=0) return; endif
  else
    ! FHJ: Open dataset
    call bgw_h5dopen(loc_id, dset_name, dset_id, error)
    if (present(error)) then; if (error/=0) return; endif
    ! FHJ: Get file dataspace
    call bgw_h5dget_space(dset_id, filespace_id, error)
    if (present(error)) then; if (error/=0) return; endif
  endif

  ! FHJ: Select hyperslab from file
  if (rank>0) then
    call bgw_h5sselect_hyperslab(filespace_id, H5S_SELECT_SET_F, hoffsetf, hcountf, error)
    if (present(error)) then; if (error/=0) return; endif
  endif

  ! FHJ: Create flat memory filespace_id
  hcountm(1) = max(1, product(countf))
  call bgw_h5screate_simple(1, hcountm, memspace_id, error)
  if (present(error)) then; if (error/=0) return; endif

  ! FHJ: read filespace_id
  call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, buf, hcountm, errcode, &
    memspace_id, filespace_id)
  call check_error('h5dread_f', errcode, error)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(memspace_id, errcode)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(filespace_id, errcode)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5dclose(dset_id, errcode)
  if (present(error)) then; if (error/=0) return; endif

  POP_SUB(hdf5_read_double_lowlevel)

end subroutine hdf5_read_double_lowlevel


!> write double to/from an HDF5 file.
subroutine hdf5_write_double_lowlevel(loc_id, dset_name, countf, buf, error, offsetf)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !! Pass (/-1/) to read/write a scalar rank-0 array.
  integer(HSIZE_T), intent(in) :: countf(:)
  !> Data buffer. We treat it as a flat contiguous 1D array.
  real(DP), intent(in), dimension(*) :: buf
  integer, intent(out), optional :: error !< error code
  !> Offset when reading dataset from file.
  integer, intent(in), optional :: offsetf(:)

  integer(HSIZE_T) :: hcountf(size(countf)) !< Count for file dataspace
  integer(HSIZE_T) :: hcountm(1) !< Count for memory dataspace
  integer(HSIZE_T) :: hoffsetf(size(countf)) !< Offset for file filespace_id
  integer(HID_T) :: dset_id, filespace_id, memspace_id
  integer :: errcode, rank
  logical :: exists

  PUSH_SUB(hdf5_write_double_lowlevel)

  if (present(error)) error = 0
  call h5lexists_f(loc_id, dset_name, exists, errcode)
  if (.not.exists .and. present(offsetf)) then
    ! FHJ: The developer should manually create the dataset, because countf
    ! is *NOT* the total size of the dataset, so we don`t have enough into to
    ! create the dataset.
    call die('Internal error: cannot automatically create dataset with "offsetf" argument.', &
      only_root_writes=.true.)
  endif

  ! FHJ: Create or get file dataspace
  if (present(offsetf)) then
    hoffsetf(:) = offsetf(:)
  else
    hoffsetf(:) = 0
  endif
  hcountf(:) = countf(:)
  rank = size(hcountf)
  if (any(countf<1)) then
    rank = 0
    ! Note: hcountf and hoffsetf are not referenced if rank==0
  endif

  if (.not.exists) then
    ! FHJ: Create file dataspace
    call bgw_h5screate_simple(rank, hcountf, filespace_id, error)
    if (present(error)) then; if (error/=0) return; endif
    ! FHJ: Create dataset
    call bgw_h5dcreate(loc_id, dset_name, H5T_NATIVE_DOUBLE, filespace_id, dset_id, error)
    if (present(error)) then; if (error/=0) return; endif
  else
    ! FHJ: Open dataset
    call bgw_h5dopen(loc_id, dset_name, dset_id, error)
    if (present(error)) then; if (error/=0) return; endif
    ! FHJ: Get file dataspace
    call bgw_h5dget_space(dset_id, filespace_id, error)
    if (present(error)) then; if (error/=0) return; endif
  endif

  ! FHJ: Select hyperslab from file
  if (rank>0) then
    call bgw_h5sselect_hyperslab(filespace_id, H5S_SELECT_SET_F, hoffsetf, hcountf, error)
    if (present(error)) then; if (error/=0) return; endif
  endif

  ! FHJ: Create flat memory filespace_id
  hcountm(1) = max(1, product(countf))
  call bgw_h5screate_simple(1, hcountm, memspace_id, error)
  if (present(error)) then; if (error/=0) return; endif

  ! FHJ: write filespace_id
  call h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, buf, hcountm, errcode, &
    memspace_id, filespace_id)
  call check_error('h5dwrite_f', errcode, error)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(memspace_id, errcode)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5sclose(filespace_id, errcode)
  if (present(error)) then; if (error/=0) return; endif
  call bgw_h5dclose(dset_id, errcode)
  if (present(error)) then; if (error/=0) return; endif

  POP_SUB(hdf5_write_double_lowlevel)

end subroutine hdf5_write_double_lowlevel



!########################################
!# LOW LEVEL routine -- LOGICAL version #
!########################################


!> read logical to/from an HDF5 file.
!! Note that this just maps the logical data to integers, and calls hdf5_read_int_lowlevel.
subroutine hdf5_read_logical_lowlevel(loc_id, dset_name, countf, buf, error, offsetf)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !! Pass (/-1/) to read/write a scalar rank-0 array.
  integer(HSIZE_T), intent(in) :: countf(:)
  !> Data buffer. We treat it as a flat contiguous 1D array.
  logical, intent(inout), dimension(*) :: buf
  integer, intent(out), optional :: error !< error code
  !> Offset when reading dataset from file.
  integer, intent(in), optional :: offsetf(:)

  integer :: buf_int(max(1, product(countf))), sz

  PUSH_SUB(hdf5_read_logical_lowlevel)

  sz = max(1, product(countf))
  call hdf5_read_int_lowlevel(loc_id, dset_name, countf, buf_int, error=error, offsetf=offsetf)
  buf(1:sz) = buf_int(:) /= 0

  POP_SUB(hdf5_read_logical_lowlevel)

end subroutine hdf5_read_logical_lowlevel

!> write logical to/from an HDF5 file.
!! Note that this just maps the logical data to integers, and calls hdf5_write_int_lowlevel.
subroutine hdf5_write_logical_lowlevel(loc_id, dset_name, countf, buf, error, offsetf)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !! Pass (/-1/) to read/write a scalar rank-0 array.
  integer(HSIZE_T), intent(in) :: countf(:)
  !> Data buffer. We treat it as a flat contiguous 1D array.
  logical, intent(in), dimension(*) :: buf
  integer, intent(out), optional :: error !< error code
  !> Offset when reading dataset from file.
  integer, intent(in), optional :: offsetf(:)

  integer :: buf_int(max(1, product(countf))), sz

  PUSH_SUB(hdf5_write_logical_lowlevel)

  sz = max(1, product(countf))
  where(buf(1:sz))
    buf_int = 1
  elsewhere
    buf_int = 0
  endwhere
  call hdf5_write_int_lowlevel(loc_id, dset_name, countf, buf_int, error=error, offsetf=offsetf)

  POP_SUB(hdf5_write_logical_lowlevel)

end subroutine hdf5_write_logical_lowlevel




!> read complex to/from an HDF5 file.
!! Note that this just maps the complex data to doubles, and calls hdf5_read_double_lowlevel.
subroutine hdf5_read_complex_lowlevel(loc_id, dset_name, countf, buf, error, offsetf)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !! Pass (/-1/) to read/write a scalar rank-0 array.
  integer(HSIZE_T), intent(in) :: countf(:)
  !> Data buffer. We treat it as a flat contiguous 1D array.
  complex(DPC), intent(inout), dimension(*), target :: buf
  integer, intent(out), optional :: error !< error code
  !> Offset when reading dataset from file.
  integer, intent(in), optional :: offsetf(:)

  integer :: rank_double
  integer(HSIZE_T) :: size_double
  integer(HSIZE_T) :: countf_double(size(countf)+1)
  integer :: offsetf_double(size(countf)+1)
  real(DP), pointer :: buf_double(:)

  PUSH_SUB(hdf5_read_complex_lowlevel)

  ! We map scalars and arrays to arrays, by adding extra dimension with size=2.
  ! Scalar is trickier, because size(countf) is meaningless.
  countf_double(1) = 2
  countf_double(2:) = countf
  offsetf_double(:) = 0
  if (any(countf<1)) then
    rank_double = 1
    size_double = 2
  else
    rank_double = 1 + size(countf)
    size_double = 2_HSIZE_T * product(countf)
  endif
  call c_f_pointer(c_loc(buf), buf_double, [size_double])
  if (present(offsetf)) then
    offsetf_double(2:) = offsetf
    call hdf5_read_double_lowlevel(loc_id, dset_name, countf_double(1:rank_double), &
      buf_double, error=error, offsetf=offsetf_double(1:rank_double))
  else
    call hdf5_read_double_lowlevel(loc_id, dset_name, countf_double(1:rank_double), &
      buf_double, error=error)
  endif

  POP_SUB(hdf5_read_complex_lowlevel)

end subroutine hdf5_read_complex_lowlevel

!> write complex to/from an HDF5 file.
!! Note that this just maps the complex data to doubles, and calls hdf5_write_double_lowlevel.
subroutine hdf5_write_complex_lowlevel(loc_id, dset_name, countf, buf, error, offsetf)
  integer(HID_T), intent(in) :: loc_id !< HDF5 file id
  character(LEN=*), intent(in) :: dset_name !< HDF5 dataset name
  !> Number of elements to read from the dataset for each dimention.
  !! Pass (/-1/) to read/write a scalar rank-0 array.
  integer(HSIZE_T), intent(in) :: countf(:)
  !> Data buffer. We treat it as a flat contiguous 1D array.
  complex(DPC), intent(in), dimension(*), target :: buf
  integer, intent(out), optional :: error !< error code
  !> Offset when reading dataset from file.
  integer, intent(in), optional :: offsetf(:)

  integer :: rank_double
  integer(HSIZE_T) :: size_double
  integer(HSIZE_T) :: countf_double(size(countf)+1)
  integer :: offsetf_double(size(countf)+1)
  real(DP), pointer :: buf_double(:)

  PUSH_SUB(hdf5_write_complex_lowlevel)

  ! We map scalars and arrays to arrays, by adding extra dimension with size=2.
  ! Scalar is trickier, because size(countf) is meaningless.
  countf_double(1) = 2
  countf_double(2:) = countf
  offsetf_double(:) = 0
  if (any(countf<1)) then
    rank_double = 1
    size_double = 2
  else
    rank_double = 1 + size(countf)
    size_double = 2_HSIZE_T * product(countf)
  endif
  call c_f_pointer(c_loc(buf), buf_double, [size_double])
  if (present(offsetf)) then
    offsetf_double(2:) = offsetf
    call hdf5_write_double_lowlevel(loc_id, dset_name, countf_double(1:rank_double), &
      buf_double, error=error, offsetf=offsetf_double(1:rank_double))
  else
    call hdf5_write_double_lowlevel(loc_id, dset_name, countf_double(1:rank_double), &
      buf_double, error=error)
  endif

  POP_SUB(hdf5_write_complex_lowlevel)

end subroutine hdf5_write_complex_lowlevel

#endif
end module hdf5_io_m
!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
